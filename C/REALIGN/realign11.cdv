/*    REALIGN11
TESTING realign CLAUSE */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

static void realign111();
static void realign112();
static void realign113();
static void realign114();
static void realign115();

static void ansyes(const char tname[]);
static void ansno(const char tname[]);

static int NL = 1000;
static int ER = 10000;
static int erri, i, j, ia, ib;

int main(int an, char **as)
{
    printf("=== START OF REALIGN11 ======================\n");
    /*  ALIGN B1[i] WITH A2[i]  REALIGN B1[i] WITH A2[2*i+8] */
    realign111();
    /* ALIGN B1[i] WITH A2[i+4]  REALIGN B1[i] WITH A2[i+8] (-i+8) */
    realign112();
    /* ALIGN B1[i] WITH A2[i+8] (-i+8) REALIGN B1[i] WITH A2[3*i-2] (i+4) */
    // realign113();
    /* ALIGN B1[i] WITH A2[2*i+8]  REALIGN B1[i] WITH A2[i] */
    realign114();
    /* ALIGN B1[ ] WITH A2[ ]  REALIGN B1[i] WITH A2[i+4] */
    realign115();
    printf("=== END OF REALIGN11 ========================\n");
    return 0;
}

/* ---------------------------------------------REALIGN111*/
/* ALIGN B1[i] WITH A2[i]  REALIGN B1[i] WITH A2[2*i+8] */
void realign111()
{
    #define AN1 25
    #define BN1 8
    int k1i = 1;
    int k2i = 0;
    int li = 0;
    int kr1i = 2;
    int kr2i = 0;
    int lri = 8;

    #pragma dvm array distribute[block]
    int A1[AN1];
    #pragma dvm array align([i] with A1[k1i * i + li])
    int B1[BN1];
    char tname[] = "realign111 ";

    erri = ER;

    #pragma dvm region out(A1, B1)
    {
    #pragma dvm parallel([i] on B1[i])
    for (i = 0; i < BN1; i++)
        B1[i] = 0;
    #pragma dvm parallel([i] on A1[i]) private(ib)
    for (i = 0; i < AN1; i++)
    {
        A1[i] = i;
        if (((i - li) == (((i - li) / k1i) * k1i)) &&
            (((i - li) / k1i) >= 0) &&
            (((i - li) / k1i) < BN1))
        {
            ib = (i - li) / k1i;
            B1[ib] = ib;
        }
    }
    }

    #pragma dvm realign(B1[i] with A1[kr1i * i + lri])
    #pragma dvm actual(erri)

    #pragma dvm region local(A1, B1)
    {
    #pragma dvm parallel([i] on B1[i]) reduction(min(erri)), private(ia)
    for (i = 0; i < BN1; i++)
    {
        if (B1[i] != i)
            if (erri > i) erri = i;
        ia = kr1i * i + lri;
        if (A1[ia] != ia)
            if (erri > i) erri = i;
    }
    }

    #pragma dvm get_actual(erri)

    if (erri == ER)
        ansyes(tname);
    else
        ansno(tname);

    #undef AN1
    #undef BN1
}

/* ---------------------------------------------REALIGN112*/
/* ALIGN B1[i] WITH A1[i+4]  REALIGN B1[i] WITH A1[i+8] (-i+8) */
void realign112()
{
    int AN1 = 16;
    int BN1 = 4;

    int k1i = 1;
    int k2i = 0;
    int li = 4;
    int kr1i = 1;
    int kr2i = 0;
    int lri = 8;
    char tname[] = "realign112 ";

    #pragma dvm array distribute[block]
    int *A1;
    #pragma dvm array
    int *B1;

    A1 = malloc(sizeof(int[AN1]));
    B1 = malloc(sizeof(int[BN1]));
    #pragma dvm realign(B1[i] with A1[k1i * i + li])

    erri = ER;

    #pragma dvm region
    {
    #pragma dvm parallel([i] on B1[i])
    for (i = 0; i < BN1; i++)
        B1[i] = 0;
    }

    #pragma dvm region inout(B1), out(A1)
    {
    #pragma dvm parallel([i] on A1[i]) private(ib)
    for (i = 0; i < AN1; i++)
    {
        A1[i] = i;
        if (((i - li) == (((i - li) / k1i) * k1i)) &&
            (((i - li) / k1i) >= 0) &&
            (((i - li) / k1i) < BN1))
        {
            ib = (i - li) / k1i;
            B1[ib] = ib;
        }
    }
    }

    #pragma dvm realign(B1[i] with A1[kr1i * i + lri])
    #pragma dvm actual(erri)

    #pragma dvm region
    {
    #pragma dvm parallel([i] on B1[i]) reduction(min(erri)), private(ia)
    for (i = 0; i < BN1; i++)
    {
        if (B1[i] != i)
            if (erri > i) erri = i;
        ia = kr1i * i + lri;
        if (A1[ia] != ia)
            if (erri > i) erri = i;
    }
    }

    #pragma dvm get_actual(erri)

    if (erri == ER)
        ansyes(tname);
    else
        ansno(tname);

    free(B1);
    free(A1);
}

/* ---------------------------------------------REALIGN113*/
/* ALIGN B1[i] WITH A1[i+8] (-i+8) REALIGN B1[i] WITH A1[3*i-2] (i+4) */
void realign113()
{
    #define AN1 30
    #define BN1 8

    int k1i = 1;
    int k2i = 0;
    int li = 8;
    int kr1i = 3;
    int kr2i = 0;
    int lri = -2;

    #pragma dvm array distribute[block]
    int A1[AN1];
    #pragma dvm array align([i] with A1[k1i * i + li])
    int B1[BN1];
    char tname[] = "realign113 ";

    erri = ER;

    #pragma dvm region
    {
    #pragma dvm parallel([i] on B1[i])
    for (i = 0; i < BN1; i++)
        B1[i] = 0;
    }

    #pragma dvm region in(B1), out(A1, B1)
    {
    #pragma dvm parallel([i] on A1[i]) private(ib)
    for (i = 0; i < AN1; i++)
    {
        A1[i] = i;
        if (((i - li) == (((i - li) / k1i) * k1i)) &&
            (((i - li) / k1i) >= 0) &&
            (((i - li) / k1i) < BN1))
        {
            ib = (i - li) / k1i;
            B1[ib] = ib;
        }
    }
    }

    #pragma dvm realign(B1[i] with A1[kr1i * i + lri])
    #pragma dvm actual(erri)

    #pragma dvm region
    {
    #pragma dvm parallel([i] on B1[i]) reduction(min(erri)), private(ia)
    for (i = 0; i < BN1; i++)
    {
        if (B1[i] != i)
            if (erri > i) erri = i;
        ia = kr1i * i + lri;
        if (A1[ia] != ia)
            if (erri > i) erri = i;
    }
    }

    #pragma dvm get_actual(erri)

    if (erri == ER)
        ansyes(tname);
    else
        ansno(tname);

    #undef AN1
    #undef BN1
}

/* ---------------------------------------------REALIGN114*/
/* ALIGN B1[i] WITH A1[2*i+8]  REALIGN B1[i] WITH A1[i] */
void realign114()
{
    int AN1 = 24;
    int BN1 = 8;

    int k1i = 2;
    int k2i = 0;
    int li = 8;
    int kr1i = 1;
    int kr2i = 0;
    int lri = 0;
    char tname[] = "realign114 ";

    #pragma dvm array distribute[block]
    int *A1;
    #pragma dvm array
    int *B1;

    A1 = malloc(sizeof(int[AN1]));
    B1 = malloc(sizeof(int[BN1]));
    #pragma dvm realign(B1[i] with A1[k1i * i + li])

    erri = ER;

    #pragma dvm region out(B1, A1)
    {
    #pragma dvm parallel([i] on B1[i])
    for (i = 0; i < BN1; i++)
        B1[i] = 0;

    #pragma dvm parallel([i] on A1[i]) private(ib)
    for (i = 0; i < AN1; i++)
    {
        A1[i] = i;
        if (((i - li) == (((i - li) / k1i) * k1i)) &&
            (((i - li) / k1i) >= 0) &&
            (((i - li) / k1i) < BN1))
        {
            ib = (i - li) / k1i;
            B1[ib] = ib;
        }
    }
    }

    #pragma dvm realign(B1[i] with A1[kr1i * i + lri])
    #pragma dvm actual(erri)

    #pragma dvm region
    {
    #pragma dvm parallel([i] on B1[i]) reduction(min(erri)), private(ia)
    for (i = 0; i < BN1; i++)
    {
        if (B1[i] != i)
            if (erri > i) erri = i;
        ia = kr1i * i + lri;
        if (A1[ia] != ia)
            if (erri > i) erri = i;
    }
    }

    #pragma dvm get_actual(erri)

    if (erri == ER)
        ansyes(tname);
    else
        ansno(tname);

    free(B1);
    free(A1);
}

/* ---------------------------------------------REALIGN115*/
/* ALIGN B1[ ] WITH A1[ ]  REALIGN B1[i] WITH A1[i+4] */
void realign115()
{
    #define AN1 24
    #define BN1 8

    int k1i = 0;
    int k2i = 0;
    int li = 0;
    int kr1i = 1;
    int kr2i = 0;
    int lri = 4;

    #pragma dvm array distribute[block]
    int A1[AN1];
    #pragma dvm array align([] with A1[])
    int B1[BN1];
    char tname[] = "realign115 ";

    erri = ER;

    for (i = 0; i < BN1; i++)
        B1[i] = i;

    #pragma dvm realign(B1[i] with A1[kr1i * i + lri])
    #pragma dvm actual(erri)

    #pragma dvm region
    {
    #pragma dvm parallel([i] on B1[i]) reduction(min(erri)), private(ia)
    for (i = 0; i < BN1; i++)
    {
        if (B1[i] != i)
            if (erri > i) erri = i;
    }
    }

    #pragma dvm get_actual(erri)

    if (erri == ER)
        ansyes(tname);
    else
        ansno(tname);

    #undef AN1
    #undef BN1
}

void ansyes(const char name[])
{
    printf("%s  -  complete\n", name);
}

void ansno(const char name[])
{
    printf("%s  -  ***error\n", name);
}

