#!/bin/sh

TEST=$1
MAX_dim=2
MAX_all=4
DVM=~/innot/dvm
OUTPUT=${TEST}_output
echo ${OUTPUT}
echo >${OUTPUT}

for((i1=1;i1<=$MAX_dim;i1++))
do
 for((i2=1;i2<=$MAX_dim;i2++))
 do
  for((i3=1;i3<=$MAX_dim;i3++))
  do
   for((i4=1;i4<=$MAX_dim;i4++))
   do
    if [ "$(($i1 * $i2 * $i3 * $i4))" -le "$MAX_all" ]; then
        $DVM run $i1 $i2 $i3 $i4 $TEST
        idx=1
        while [ "1" == "1" ]
        do
            list=`mps|grep $TEST |sed 's/^[0123456789]\+. //'  | sed 's#.'queued'##g' `

            if [ "$idx" -eq "1" ]; then
                echo $list 1 >> ${OUTPUT}
                p=$list
                idx=$(($idx+1))    
                echo run on grid "$i1 $i2 $i3 $i4" \( $p \) 1 >>${OUTPUT}
            fi

            if [ "$list" == "" ]; then
                break
            else
                echo "  sleep 10"
                sleep 10
            fi
        done
        time_res=`cat $p/output | grep "Time in" | sed 's/^.\+\?=\s*//'`
        check=`cat $p/output | grep SUC`
        echo $time_res $check 1 >>${OUTPUT}
    fi

   done
  done
 done  
done
